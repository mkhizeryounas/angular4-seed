import { WadPage } from './app.po';

describe('wad App', function() {
  let page: WadPage;

  beforeEach(() => {
    page = new WadPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AboutComponent } from './components/about/about.component';
import { ServicesComponent } from './components/services/services.component';
import { ClarifaiComponent } from './components/clarifai/clarifai.component';

export const router: Routes = [
    {
        path: "",
        redirectTo: "about",
        pathMatch: "full"
    },
    {
        path: "about",
        component: AboutComponent
    },
    {
        path: "services",
        component: ServicesComponent
    },
    {
        path: "clarifai",
        component: ClarifaiComponent
    },
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);

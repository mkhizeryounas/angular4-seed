import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  constructor(private zone: NgZone) { 
    // this.zone.run(() => {}); // $scope.apply();
  }
  
  ngOnInit() {
  }

}

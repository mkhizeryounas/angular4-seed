/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ClarifaiComponent } from './clarifai.component';

describe('ClarifaiComponent', () => {
  let component: ClarifaiComponent;
  let fixture: ComponentFixture<ClarifaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClarifaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClarifaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, NgZone } from '@angular/core';
const Clarifai = require('clarifai');

@Component({
  selector: 'app-clarifai',
  templateUrl: './clarifai.component.html',
  styleUrls: ['./clarifai.component.css']
})
export class ClarifaiComponent implements OnInit {

  constructor(private zone: NgZone) { }
  link:string = "https://samples.clarifai.com/metro-north.jpg";
  cImg:string;
  prediction:any;

  predict(l) {
    const app = new Clarifai.App({
      apiKey: 'ac769f524f1b447fa74f17b24b54b8ac'
    });
    app.models.predict(Clarifai.GENERAL_MODEL, l).then((res)=>{
      this.prediction = res.outputs[0].data.concepts;
      console.log(this.prediction);
      this.cImg = l;
      this.zone.run(() => {});
    });
    
  }
  ngOnInit() {
  }

}
